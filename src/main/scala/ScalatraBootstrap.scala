import javax.servlet.ServletContext
import org.scalatra._
import com.qa.MainServlet

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {

    // Mount servlets.some stuff
    context.mount(new MainServlet, "/*")


  }
}
